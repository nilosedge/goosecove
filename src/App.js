import './App.css';
import React, { useState, useEffect } from 'react';
import Plot from 'react-plotly.js';
import data2020 from './2020.json';
import data2021 from './2021.json';
import data2022 from './2022.json';
import data2023 from './2023.json';

function App() {

	const [chartData, setChartData] = useState([]);

	var layout = {
		width: 1200, height: 1000
	};

	return (
		<div className="App">
			<header className="App-header">
				<Plot data = {[ data2020 ]} layout = { layout } />
				<Plot data = {[ data2021 ]} layout = { layout } />
				<Plot data = {[ data2022 ]} layout = { layout } />
				<Plot data = {[ data2023 ]} layout = { layout } />
			</header>
		</div>
	);
}

export default App;
